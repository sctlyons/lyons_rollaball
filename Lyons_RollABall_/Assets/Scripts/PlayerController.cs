﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float Speed;

    public Text countText;

    public Text winText;

    public Text loseText;

    private Rigidbody rb;

    private int count;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        count = 0; //score is set to 0
        SetCountText ();
        winText.text = ""; //Win text is blank at start
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * Speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false); //sets triggered pick up to inactive
            count = count + 1; //add one to score
            SetCountText (); //call SetCountText function
        }

    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString ();
        if (count >= 13)
        {
            winText.text = "YOU WIN"; //dsiplays YOU WIN once 13 pickups have been triggered

            Invoke("RestartLevel", 2f); //call level restart after 2 secs

        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene("RollABall"); //reload RollABall scene
    }
}
