﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class KillCube : MonoBehaviour
{

    public int LoseValue = 0;//creates LoseValue variable
    GameObject player;//Reference to player game object
    Rigidbody rb;//Reference to the rigidbody
    public float speed; //how fast the cubes move
    public Text loseText;
    // Start is called before the first frame update
    void Start()
    {
        loseText.text = ""; //lose text is blank
        player = GameObject.FindGameObjectWithTag("Player"); //search for gameobject tagged "Player"
        rb = GetComponent<Rigidbody>();//find rigidbody and save a reference to it
    }

    // Update is called once per frame
    void Update()
    {
        if (LoseValue > 0 && Input.GetKeyDown(KeyCode.R)) //when LoseValue is greater than 0 and the "R" key is pressed
        {
            SceneManager.LoadScene("RollABall"); //restart RollABall scene
        }
    }

    private void FixedUpdate()
    {
        //follow the Player
        Vector3 dirToPlayer = player.transform.position - transform.position; //what is position between KillCube and player
        dirToPlayer = dirToPlayer.normalized;//convert magnitude of vector to 1

        rb.AddForce(dirToPlayer * speed); //move in direction
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.SetActive(false); //destroys Playerobject
            loseText.text = "Press R to Restart"; //displays message to restart game
            LoseValue++; //adds one to lose value

        }
    }
}